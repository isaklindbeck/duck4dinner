﻿using UnityEngine;
using System.Collections;

public class EyeTracking : MonoBehaviour {
	private EyeXGazePointProvider _gazePointProvider;
	
	#if UNITY_EDITOR
	private EyeXGazePointType _oldGazePointType;
	#endif
	
	/// <summary>
	/// Choice of gaze point data stream to be visualized.
	/// </summary>
	public EyeXGazePointType gazePointType = EyeXGazePointType.GazeLightlyFiltered;

	/// <summary>
	/// The size of the visualizer point
	/// </summary>
	public float pointSize = 5;
	public Transform projectile;
	/// <summary>
	/// The color of the visualizer point
	/// </summary>
	public Color pointColor = Color.yellow;

	private float maxH = 2.5f;
	private float minH = 0.0f;

	private Vector3 lastTarget;
	private float targetTTL = 0;

	public Transform marker;
	private EyeXGazePoint lastGP;

	private Vector3 lastCamPos;

	private Vector3 targetObjPos;
	private Vector3 lastTargetObjPos;

	private EyeXGazePoint[] points;
	private int index = 0;

	public void Awake()
	{
		_gazePointProvider = EyeXGazePointProvider.GetInstance();

		points = new EyeXGazePoint[80];

		#if UNITY_EDITOR
		_oldGazePointType = gazePointType;
		#endif
	}

	public void Update() {
		var gazePoint = getGazePoint();

		Vector3 direction;

		Vector3 target = getTarget();
		lastTarget += targetObjPos - lastTargetObjPos;
		float dist = (lastGP.Viewport - gazePoint.Viewport).magnitude;
		//Debug.Log(dist + " l: " + lastGP.Viewport + " c: " + gazePoint.Viewport);

		float TTL = 1.0f;
		if(dist <= 0.0f) {
			target = lastTarget;
			
		} else if(Vector3.Angle(target - transform.position, lastTarget - transform.position) < 4f) {
			target = lastTarget;

			if(targetTTL < 300 * TTL)
				targetTTL += 30f * TTL;

		} else {
			Vector3 v1 = Camera.main.WorldToViewportPoint(target);
			v1.z = 0;
			lastCamPos.z = 0;
			float diff = (v1 - lastCamPos).magnitude;
			targetTTL -= 1000 * diff * diff;

			if(targetTTL <= 0) {
				targetTTL = TTL;
			} else {
				target = lastTarget;
			}
		}

		lastCamPos = Camera.main.WorldToViewportPoint(target);
		lastTarget = target;
		lastTargetObjPos = targetObjPos;
		lastGP = gazePoint;
		//marker.position = target;

		if(Input.GetKeyDown(KeyCode.RightControl)) {
			Screen.lockCursor = true;
			direction = target - transform.TransformPoint(new Vector3(0, 0, 1.0f));
			Instantiate(projectile, transform.TransformPoint(new Vector3(0, 0, 1.0f)), Quaternion.LookRotation(direction));
		}


		float rotate = 0;
		float f = 0.4f;

		float x = gazePoint.Viewport.x;
		float y = gazePoint.Viewport.y;

		if(x <= f) {
			rotate = ((f - x) / f);
			rotate *= rotate;
			rotate *= (-120f);
		}
		if(x >= 1.0 - f) {
			rotate = ((f - (1.0f - x)) / f);
			rotate *= rotate;
			rotate *= 120f;
		}
		transform.Rotate(0, rotate * Time.deltaTime, 0);
		
		float move = 0f;
		float cameraH = Camera.main.transform.position.y - transform.position.y;
		if(y <= f && cameraH <= maxH) {
			move = (f - y) / f;
			move *= move;
		}
		if(y >= 1.0f - f && cameraH >= minH) {
			move = (f - (1.0f - y)) /  f;
			move *= -move;
		}
		move *= 3f;
		float nextH = cameraH + move * Time.deltaTime;
		if(nextH > maxH || nextH < minH) {
			move = 0;
		}
		Camera.main.transform.Translate(Vector3.up * move * Time.deltaTime);
		Vector3 lookpoint = transform.position + Vector3.up;
		Camera.main.transform.LookAt(lookpoint);
		

		if (Input.GetKeyDown (KeyCode.M))
			showGUI = !showGUI;
	}

	public Vector3 getTarget() {
		var gazePoint = getGazePoint();
		Ray ray = Camera.main.ScreenPointToRay (new Vector3(gazePoint.Screen.x, gazePoint.Screen.y, 1));
		RaycastHit hit;

		int prio = 0;
		float dist = 0f;

		bool b = Physics.Raycast(ray, out hit);
		Vector3 target = (ray.origin + ray.direction * 500);
		if(b) {
			target = hit.point;
			targetObjPos = hit.transform.position;
			dist = hit.distance;
			AimPriority aimscript  = hit.transform.gameObject.GetComponent<AimPriority>();
			prio = aimscript.priority;
		} else {
			dist = 500;
		}

		Vector3 origin = Camera.main.ScreenToWorldPoint(new Vector3(gazePoint.Screen.x, gazePoint.Screen.y, 1));
		Vector3 forward = ray.direction;
		Vector3 orth = Vector3.Cross (ray.direction, Vector3.up);

		for(int k = 0; k < 8; k++) {

			ray.direction = Quaternion.AngleAxis (0.5f, orth) * ray.direction;
			ray.direction = Quaternion.AngleAxis (5f, forward) * ray.direction;
			for(int i = 0; i < 8; i++) {
				ray.direction = Quaternion.AngleAxis (45f, forward) * ray.direction;
				b = Physics.Raycast(ray, out hit);
				if(b) {
					AimPriority aimscript  = hit.transform.gameObject.GetComponent<AimPriority>();
					int newprio = aimscript.priority;
					if (newprio > prio) {
						target = hit.point;
						targetObjPos = hit.transform.position;
						prio = newprio;
						dist = hit.distance;
					} else if(newprio == prio && hit.distance < dist) {
						target = hit.point;
						targetObjPos = hit.transform.position;
						prio = newprio;
						dist = hit.distance;
					}
				}
			}

		}

		return target;
	}
	
	public void OnEnable()
	{
		_gazePointProvider.StartStreaming(gazePointType);
	}
	
	public void OnDisable()
	{
		_gazePointProvider.StopStreaming(gazePointType);
	}


	private bool showGUI = false;

	/// <summary>
	/// Draw a GUI.Box at the user's gaze point.
	/// </summary>
	public void OnGUI()
	{

		if (!showGUI)
			return;


		#if UNITY_EDITOR
		if (_oldGazePointType != gazePointType)
		{
			_gazePointProvider.StopStreaming(_oldGazePointType);
			_oldGazePointType = gazePointType;
			_gazePointProvider.StartStreaming(gazePointType);
		}
		#endif
		
		var defaultStyle = GUI.skin.box;
		GUI.skin.box = CreateBoxStyle();
		
		var title = "";
		
		// Show fixation index for fixation types
		if (gazePointType == EyeXGazePointType.FixationSensitive || gazePointType == EyeXGazePointType.FixationSlow)
		{
			var fixationIndex = _gazePointProvider.GetLastFixationCount(gazePointType);
			title = fixationIndex.ToString();
		}
		
		var gazePoint = getGazePoint ();

		if (gazePoint.IsWithinScreenBounds)
		{
			GUI.Box(new UnityEngine.Rect(gazePoint.GUI.x - pointSize / 2.0f, gazePoint.GUI.y - pointSize / 2.0f, pointSize, pointSize), title);
			//GUI.Box(new UnityEngine.Rect(Camera.main.WorldToScreenPoint(lastTarget).x - pointSize / 2.0f, Camera.main.WorldToScreenPoint(lastTarget).y - pointSize / 2.0f, pointSize, pointSize), title);
		}
		
		GUI.skin.box = defaultStyle;

	}

	private EyeXGazePoint getGazePoint() {
		EyeXGazePoint gazePoint = _gazePointProvider.GetLastGazePoint(gazePointType);

		points[index] = gazePoint;
		
		
		EyeXGazePoint minPoint = gazePoint;
		float minDist = float.MaxValue;
		
		for(int i = 0; i < points.Length; i++) {
			float dist = 0;
			for(int k = 0; k < points.Length; k++) {
				if(i!=k) {
					Vector2 v = points[i].Screen - points[k].Screen;
					dist += v.sqrMagnitude;
				}
			}
			if(dist < minDist) {
				minPoint = points[i];
				minDist = dist;
			}
		}
		
		float currentDist = (gazePoint.Screen - minPoint.Screen).sqrMagnitude;
		if ((minDist / points.Length - 1.0f) / currentDist > 0.8f) {
			for(int i = 0; i < points.Length; i++) {
				points[i] = gazePoint;
			}
		} else {
			gazePoint = minPoint;
			//points[index] = gazePoint;
		}
		index = (index + 1) % points.Length;


		return gazePoint;
	}
	
	private GUIStyle CreateBoxStyle()
	{
		var style = new GUIStyle(GUI.skin.box);
		
		var texture = new Texture2D(1, 1);
		texture.SetPixel(0, 0, pointColor);
		texture.Apply();
		style.normal.background = texture;
		
		style.border = new RectOffset(0, 0, 0, 0);
		
		return style;
	}
}
