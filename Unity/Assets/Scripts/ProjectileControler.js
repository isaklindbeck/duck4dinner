﻿#pragma strict

var projectileSpeed = 5.0;
var TTL = 10.0;

private var lastPosition : Vector3;

function Start() {
	lastPosition = transform.position;
}

function Update () {
	
	transform.Translate(Vector3.forward * Time.deltaTime * projectileSpeed);
    var direction = transform.position - lastPosition;
    var hit : RaycastHit;
    if (Physics.Raycast(lastPosition, direction, hit, direction.magnitude)) {        
        if(hit.transform.gameObject.name.Contains("duck")) {
        	hit.transform.gameObject.GetComponent(AiControl).getHit();
        }
        
        Destroy(gameObject);
    }

    this.lastPosition = transform.position;
    
    TTL -= Time.deltaTime;
    if(TTL < 0) {
        Destroy(gameObject);
    }
}