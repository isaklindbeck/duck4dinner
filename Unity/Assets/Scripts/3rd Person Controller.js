﻿@script RequireComponent(CharacterController)
private var collisionFlags : CollisionFlags; 

var playerSpeed = 10.0;
var gravity = 20.0;
var verticalSpeed = 0.0;
var rotateSpeed = 180;
var jumpSpeed = 10;

public var idleAnimation : AnimationClip;
public var walkAnimation : AnimationClip;
public var runAnimation : AnimationClip;
public var jumpPoseAnimation : AnimationClip;

var animator : Animator;

public var jumpAudio : AudioClip;
public var runAudio : AudioClip;
var stepFreq = 3.0;
var currentStepTime = 0.0;

var projectile:Transform;



public var walkMaxAnimationSpeed : float = 0.75;
public var trotMaxAnimationSpeed : float = 1.0;
public var runMaxAnimationSpeed : float = 1.0;
public var jumpAnimationSpeed : float = 1.15;
public var landAnimationSpeed : float = 1.0;

private var _animation : Animation;

enum CharacterState {
	Idle = 0,
	Walking = 1,
	Trotting = 2,
	Running = 3,
	Jumping = 4,
}

private var cs : CharacterState;
private var jumping = false;
private var jumpingReachedApex = false;

function Start() {
	animator = transform.GetComponentInChildren(Animator);
}


function Update() {
	var dir = new Vector3(0,0,0);
	cs = CharacterState.Idle;
	if (Input.GetKey (KeyCode.W)) {
		dir = dir + new Vector3(0,0,1);
		cs = CharacterState.Running;
	}
	if (Input.GetKey (KeyCode.A)) {
		dir = dir + new Vector3(-1,0,0);
		cs = CharacterState.Running;
	}
	if (Input.GetKey (KeyCode.S)) {
		dir = dir + new Vector3(0,0,-1);
		cs = CharacterState.Running;
	}
	if (Input.GetKey (KeyCode.D)) {
		dir = dir + new Vector3(1,0,0);
		cs = CharacterState.Running;
	}
	dir = dir.normalized;
	dir = transform.TransformDirection(dir);
	
	if(dir.magnitude > 0.1 && IsGrounded() && currentStepTime < 0) {
		audio.volume = 0.02;
		audio.PlayOneShot(runAudio);
		currentStepTime = 0.4;
	}
	currentStepTime -= Time.deltaTime;

	
	applyGravity();
	
	if(Input.GetKey(KeyCode.Space) && IsGrounded()) {
		verticalSpeed += jumpSpeed;
		jumping = true;
		jumpingReachedApex = false;
		cs = CharacterState.Jumping;
		audio.volume = 0.02;
		audio.PlayOneShot(jumpAudio);
	}
	
	animator.SetFloat ("Forward", dir.magnitude * 0.8f, 0.0f, Time.deltaTime);
	//animator.SetFloat ("Turn", dir.x * 0.5 , 0.1f, Time.deltaTime);
	animator.SetBool ("OnGround", !jumping);
	animator.SetFloat ("Jump", verticalSpeed);
	
	var vs = new Vector3(0, verticalSpeed,0);
	var movement = (dir * playerSpeed + vs) * Time.deltaTime;
	
	
	var controller : CharacterController = GetComponent(CharacterController);
	collisionFlags = controller.Move(movement);

/*
	if (Input.GetKey (KeyCode.LeftArrow)) {
		transform.Rotate(0, -rotateSpeed * Time.deltaTime, 0);
	}
	if (Input.GetKey (KeyCode.RightArrow)) {
		transform.Rotate(0, rotateSpeed * Time.deltaTime, 0);
	}
*/


	//transform.Rotate(0, rotateSpeed * Input.GetAxis("Mouse X") * Time.deltaTime, 0);
	
	/*
	if(Input.GetKeyDown(KeyCode.Mouse0)) {
		Screen.lockCursor = true;
		var cam : Transform = Camera.main.transform;
		var ray = new Ray(cam.position, cam.forward);
		var hit : RaycastHit;
		var direction;
		if (Physics.Raycast(ray, hit, 500)) {
			
			direction = hit.point - transform.TransformPoint(0, 0, 1.0);
		} else {
			direction = cam.forward * 500 - transform.TransformPoint(0, 0, 1.0);	
		}
		
		if(direction.magnitude < 2.0)
			direction = cam.forward * 500 - transform.TransformPoint(0, 0, 1.0);
		
		Instantiate(projectile, transform.TransformPoint(0, 0, 1.0), Quaternion.LookRotation(direction));
	}
	*/
	
	if(_animation) {
		if(cs == CharacterState.Jumping) 
		{
			if(!jumpingReachedApex) {
				_animation[jumpPoseAnimation.name].speed = jumpAnimationSpeed;
				_animation[jumpPoseAnimation.name].wrapMode = WrapMode.ClampForever;
				_animation.CrossFade(jumpPoseAnimation.name);
			} else {
				_animation[jumpPoseAnimation.name].speed = -landAnimationSpeed;
				_animation[jumpPoseAnimation.name].wrapMode = WrapMode.ClampForever;
				_animation.CrossFade(jumpPoseAnimation.name);				
			}
		} 
		else 
		{
			var speed = dir.sqrMagnitude;
			if(cs == CharacterState.Idle) {
				_animation.CrossFade(idleAnimation.name);
			}
			else 
			{
				if(cs == CharacterState.Running) {
					_animation[runAnimation.name].speed = Mathf.Clamp(speed, 0.0, runMaxAnimationSpeed);
					_animation.CrossFade(runAnimation.name);	
				}				
			}
		}
	}
}

function IsGrounded () {
	return (collisionFlags & CollisionFlags.CollidedBelow) != 0;
}

function applyGravity() {

	if (jumping && !jumpingReachedApex && verticalSpeed <= 0.0) {
		jumpingReachedApex = true;
	}
	
	

	if (IsGrounded ()) {
		verticalSpeed = 0.0;
		jumping = false;
	} else {
		verticalSpeed -= gravity * Time.deltaTime;
		
		if(jumping)
			cs = CharacterState.Jumping;
	}
}

function Awake ()
{
	moveDirection = transform.TransformDirection(Vector3.forward);
	Screen.lockCursor = true;
	_animation = GetComponent(Animation);
	if(!_animation)
		Debug.Log("The character you would like to control doesn't have animations. Moving her might look weird.");
	
	/*
public var idleAnimation : AnimationClip;
public var walkAnimation : AnimationClip;
public var runAnimation : AnimationClip;
public var jumpPoseAnimation : AnimationClip;	
	*/
	if(!idleAnimation) {
		_animation = null;
		Debug.Log("No idle animation found. Turning off animations.");
	}
	if(!walkAnimation) {
		_animation = null;
		Debug.Log("No walk animation found. Turning off animations.");
	}
	if(!runAnimation) {
		_animation = null;
		Debug.Log("No run animation found. Turning off animations.");
	}
	if(!jumpPoseAnimation) {
		_animation = null;
		Debug.Log("No jump animation found. Turning off animations.");
	}
			
}