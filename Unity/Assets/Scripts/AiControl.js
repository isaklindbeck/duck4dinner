﻿#pragma strict
@script RequireComponent(CharacterController)

private var collisionFlags : CollisionFlags; 

var patrolWayPoints : Transform[];                   // An array of transforms for the patrol route.
var index = 0;
var speed = 1.0;

var aiSpeed = 2.0;
var gravity = 20.0;
var verticalSpeed = 0.0;

var health = 100;

var corps:Transform;

function Awake() {

}

function getHit() {
	health -= 10;
	Debug.Log("You hit a duck!! HP: " + health);
	if(health <= 0) {
		Destroy(gameObject);
		var obj = Instantiate(corps, transform.TransformPoint(0, 0, 0), Quaternion.LookRotation(transform.forward)) as GameObject;
		//obj.transform.localScale = 3f * transform.localScale / 0.4f;		
		
		var ui = GameObject.Find("GameUI") as GameObject;
		Debug.Log("1");
		var script : GameControl = ui.GetComponent(GameControl);
		script.tryEnd();
		Debug.Log("2");
	}
}

function Update ()
{
	var dir = new Vector3(0,0,0);
    if(false)
        dir = Shooting();
    else if(false)
    	dir = Chasing();
    else
        dir = Patrolling();

	dir = dir.normalized;

	applyGravity();
	var vs = new Vector3(0, verticalSpeed,0);
	
	var movement = (dir * aiSpeed + vs) * Time.deltaTime;

  	var controller : CharacterController = GetComponent(CharacterController);
	collisionFlags = controller.Move(movement);
	
	//transform.LookAt(transform.position + dir, Vector3.up);
	//transform.rotation = Quaternion.LookRotation(dir);
}


function Shooting (){
	return Vector3.zero;
}


function Chasing (){
	return Vector3.zero;
}


function Patrolling (){
	var pos = transform.position;
	var look = patrolWayPoints[index].position;
	look.y = transform.position.y;
	//transform.LookAt(look, Vector3.up);
	
	var dir = patrolWayPoints[index].position - pos;
	dir.y = 0;
	if(dir.magnitude < 0.2) {
		index = (index + 1) % patrolWayPoints.Length;
	}
	//transform.LookAt(transform.position + dir, Vector3.up);
	transform.forward = dir;
	return transform.forward;
}

function IsGrounded () {
	return (collisionFlags & CollisionFlags.CollidedBelow) != 0;
}

function applyGravity() {
	if (IsGrounded ())
		verticalSpeed = 0.0;
	else
		verticalSpeed -= gravity * Time.deltaTime;
}
