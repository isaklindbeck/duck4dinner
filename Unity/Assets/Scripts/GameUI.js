﻿#pragma strict

function Start () {
	PlayerPrefs.SetInt("score", 0);
}

function Update () {
	if(Input.GetKeyDown(KeyCode.Escape) == true) {
		Application.LoadLevel("menu");
	}
}

function OnGUI() {
	var s = PlayerPrefs.GetInt("score");
	GUI.Box (new UnityEngine.Rect(0,0,100,20), "Score: " + s.ToString());	
}

function OnDestroy () {
	var s = PlayerPrefs.GetInt("score");
	for(var i = 1; i <= 10; i++) {
		var hs = PlayerPrefs.GetInt("HighScore" + i);
		if(s > hs) {
			PlayerPrefs.SetInt("HighScore" + i, s);
			PlayerPrefs.SetString("TopPlayer" + i, PlayerPrefs.GetString("playerName"));
			break;
		}
		
	}
}