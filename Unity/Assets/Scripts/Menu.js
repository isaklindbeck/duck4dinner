﻿#pragma strict
var playerName : String = "AAA";

function Update () {
	if(Input.GetKeyDown(KeyCode.Delete) == true) {
		PlayerPrefs.DeleteAll();
	}
}

function Awake() {
	Screen.lockCursor = false;
}

function OnGUI() {

	playerName = GUI.TextField(Rect(Screen.width / 2.0 - 400, Screen.height / 2.0 - 200,400,20), playerName, 25);
	
	if (GUI.Button (Rect(Screen.width / 2.0 - 400, Screen.height / 2.0 - 160,400,150), "Start")) {
		PlayerPrefs.SetString("playerName", playerName);
		Application.LoadLevel("game");
	}

	if (GUI.Button (Rect(Screen.width / 2.0 - 400, Screen.height / 2.0 + 50,400,150), "Exit")) {
		Debug.Log("Exit");
		Application.Quit();
	}
	
	
	var text = "HighScore\n";
	
	for(var i = 1; i <= 10; i++) {
		text += "\n" + PlayerPrefs.GetFloat("HighScore" + i) + ",   " + PlayerPrefs.GetString("TopPlayer"+i); 
	}

	var style = GUI.skin.GetStyle("Label");
	style.alignment = TextAnchor.UpperLeft;
	GUI.Box (new UnityEngine.Rect(Screen.width / 2.0 + 100, Screen.height / 2.0 - 200,400,400), text, style);	
}