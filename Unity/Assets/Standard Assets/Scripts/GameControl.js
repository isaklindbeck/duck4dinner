﻿#pragma strict

private var start = 0f;
private var end = 0f;
private var ducks = 5;


function Start () {
	PlayerPrefs.SetInt("score", 0);
}

function Awake() {
	start = Time.time;

}

function Update () {
	if(Input.GetKeyDown(KeyCode.Escape) == true) {
		Application.LoadLevel("menu");
	}
}

function OnGUI() {
	var s = PlayerPrefs.GetInt("score");
	if(end == 0f) {
		GUI.Box (new UnityEngine.Rect(0,0,100,20), "Time: " + (Time.time - start));
	} else {
		GUI.Box (new UnityEngine.Rect(0,0,100,20), "Time: " + (end - start));
	}
	
	GUI.Box (new UnityEngine.Rect(0,25,100,20), "Ducks left: " + ducks);	
	
	if(end) {
		GUI.Box (new UnityEngine.Rect(Screen.width / 2f - 150,Screen.height / 2f - 10,300,20), "You Won, press ESC to return to the menu.");
	}	
}

function tryEnd() {
	ducks--;
	if(ducks <= 0) {
		end = Time.time;
		var res = end - start;
		for(var i = 1; i <= 10; i++) {
			var hs = PlayerPrefs.GetFloat("HighScore" + i);
			if(res < hs || hs == 0f) {
				for(var k = 9; k > i; k--) {
					if(PlayerPrefs.GetFloat("HighScore" + i) == 0f)
						break;
					
					PlayerPrefs.SetFloat("HighScore" + (k+1), PlayerPrefs.GetFloat("HighScore" + k));
					PlayerPrefs.SetString("TopPlayer" + (k+1), PlayerPrefs.GetString("TopPlayer"+k));
				}
				
				PlayerPrefs.SetFloat("HighScore" + i, res);
				PlayerPrefs.SetString("TopPlayer" + i, PlayerPrefs.GetString("playerName"));
				
				break;
			}
		}
	}
}

function OnDestroy () {

}